/* Copyright 2018 IPB, Universite de Bordeaux, INRIA & CNRS
**
** This file is part of the Scotch software package for static mapping,
** graph partitioning and sparse matrix ordering.
**
** This software is governed by the CeCILL-C license under French law
** and abiding by the rules of distribution of free software. You can
** use, modify and/or redistribute the software under the terms of the
** CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
** URL: "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided
** only with a limited warranty and the software's author, the holder of
** the economic rights, and the successive licensors have only limited
** liability.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL-C license and that you accept its terms.
*/
/************************************************************/
/**                                                        **/
/**   NAME       : context.h                               **/
/**                                                        **/
/**   AUTHOR     : Francois PELLEGRINI                     **/
/**                                                        **/
/**   FUNCTION   : These lines are the data declarations   **/
/**                for the execution context handling      **/
/**                routines.                               **/
/**                                                        **/
/**   DATES      : # Version 6.1  : from : 04 aug 2018     **/
/**                                 to     12 aug 2018     **/
/**                                                        **/
/************************************************************/

#define CONTEXT_H

/*
**  The defines.
*/

/*+ Context option flags. +*/

#define CONTEXTNONE                 0x0000        /*+ No options set      +*/

#define CONTEXTFREETHREAD           0x0001        /*+ Free thread context +*/
#define CONTEXTFREERANDOM           0x0002        /*+ Free random context +*/

/*
**  The type and structure definitions.
*/

/*+ The context flag type. +*/

typedef int ContextFlag;                          /*+ Context property flags +*/

/*+ The context structure. +*/

typedef struct Context_ {
  ContextFlag               flagval;              /*+ Context properties +*/
  ThreadContext             thrddat;              /*+ Threading context  +*/
} Context;

/*+ The context container structure. It
    simulates the shape of objects to which
    a context is attached.                  +*/

typedef struct ContextContainer_ {
  int                       flagval;              /*+ TRICK: flag area similar to that of attached objects +*/
  Context *                 contptr;              /*+ Context attached to the object                       +*/
  void *                    dataptr;              /*+ Object to which the context is attached              +*/
} ContextContainer;

/*
**  The function prototypes.
*/

/*
**  The macro definitions.
*/

#define containerContext(c)         (((ContainerContext *) (c))->contptr)
#define containerObject(c)          (((ContainerContext *) (c))->dataptr)

#define CONTEXTGET(cp, t, f, op1, op2)   
  t *                 op2;               \
  Context             cp##dat;           \
  Context *           cp;                \
  op2 = (t *) op1;                       \
  if (f (op2)) {                         \
    cp  = contextContainerContext (op2); \
    op2 = contextContainerObject  (op2); \
  }                                      \
  else {                                 \
    contptr = contextInit (&cp##dat);    \
    contptr = &cp##dat;                  \
  }

#define CONTEXTRELEASE(cp) \
  if (cp == &cp#dat)
    contextExit (cp)
